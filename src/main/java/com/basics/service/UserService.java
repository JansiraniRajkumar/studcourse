package com.basics.service;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.basics.model.Course;
import com.basics.model.Student;

@Service
public class UserService {

	@Autowired
	RestTemplate restTemplate;

	@Bean
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	public String getAllStudents() {
		String result = restTemplate.getForObject("http://localhost:8072/demo/students/student5", String.class);
		return result;
	}
	
public String getAllCourses() {
		String result = restTemplate.getForObject("http://localhost:8070/studentmap/courses/course", String.class);
		return result;
	}

	public String getStudentById(String userId) {
		String url = "http://localhost:8072/demo/students/student1/" + userId;
		String result = restTemplate.getForObject(url, String.class);
		return result;
	}
	
	

	public ResponseEntity<String> createStudent(Student student) {
		String uri = "http://localhost:8072/demo/students/student";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();

		request.put("studentId", student.getStudentId());
		request.put("studentName", student.getStudentName());
	

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		System.out.println(response);
		return response;
	}
	
	
	public String getCourseById(String userId) {
		String url = "http://localhost:8070/studentmap/courses/course4/" + userId;
		String result = restTemplate.getForObject(url, String.class);
		return result;
	}


	

	public ResponseEntity<String> createCourse(Course course) {
		String uri = "http://localhost:8084/studentmap/courses/course";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("courseId", course.getCourseId());
		request.put("name", course.getName());
		request.put("description",course.getDescription());

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		System.out.println(response);
		return response;
	}



}
