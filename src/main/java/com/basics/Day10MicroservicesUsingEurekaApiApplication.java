package com.basics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Day10MicroservicesUsingEurekaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day10MicroservicesUsingEurekaApiApplication.class, args);
	}

}
