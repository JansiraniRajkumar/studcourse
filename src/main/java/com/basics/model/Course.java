package com.basics.model;

import java.util.Set;




public class Course {

	
	private long courseId;
	private String name;
	private String description;
	private Set<Student> student;

	public Course() {
		super();
	}

	public Course(long courseId, String name, String description, Set<Student> student) {
		super();
		this.courseId = courseId;
		this.name = name;
		this.description = description;
		this.student = student;
	}
	
	

	public Course(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public Course(long courseId, String name, String description) {
		super();
		this.courseId = courseId;
		this.name = name;
		this.description = description;
	}

	public long getCourseId() {
		return courseId;
	}

	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Student> getStudent() {
		return student;
	}

	public void setStudent(Set<Student> student) {
		this.student = student;
	}

	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", name=" + name + ", description=" + description + ", student="
				+ student + "]";
	}

	
}
