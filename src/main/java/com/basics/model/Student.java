package com.basics.model;

import java.util.Set;

public class Student {
	
	private long studentId;
	private String studentName;
	private Set<Course> courses;

	public Student() {
		super();
	}

	public Student(String studentName) {
		super();
		this.studentName = studentName;
	}

	public Student(long studentId, String studentName, Set<Course> courses) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.courses = courses;
	}

	public Student(long studentId, String studentName) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ", courses=" + courses + "]";
	}

	public void setCourses(int i, String string, String string2, int j) {
		// TODO Auto-generated method stub

	}

}
