package com.basics.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.basics.model.Course;
import com.basics.model.Student;
import com.basics.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping("")
	public String getStudents() {
		return userService.getAllStudents();
	}


	@GetMapping("/{userId}")
	public String getStudentById(@PathVariable String userId) {
		return userService.getStudentById(userId);
	}


	@PostMapping("/body")
	public ResponseEntity<String> createStudent(Student student) {
		return userService.createStudent(student);
	}
	

	@GetMapping("/course3")
	public String getCourses() {
		return userService.getAllCourses();
	}
	
	@GetMapping("/course9/{userId}")
	public String getCourseById(@PathVariable String userId) {
		return userService.getCourseById(userId);
	}

	@PostMapping("/course8")
	public ResponseEntity<String> createCourse(Course course) {
		return userService.createCourse(course);
	}
	
}
